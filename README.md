# README

## Mission Statement

The purpose of Dreamer Labs is to:

- Create high-quality code, with an emphasis on infrastructure-as-code
- Contribute that code, free-of-charge, back to the larger open source community


## Governance

Dreamer Labs is an organization hosting a set of open source projects that are sponsored by Global InfoTek, Inc, but also encourages participation from outside contributors in the open source community. Anyone may be an active community member of Dreamer Labs by doing things like: submitting code, participating in technical steering comittee meetings, submitting issues for consideration by project Maintainers, or any other type of helpful contribution they see fit. This document describes how various types of community members contribute to the organization's mission.

## Roles and Responsibilities

### Users

Users are community members who have a need for a Dreamer Labs project. Anyone can be a User; there are no special requirements. Common User contributions include evangelizing a project or the organization (e.g., displaying a link on a website and raising awareness through word-of-mouth), informing developers of strengths and weaknesses from a new user perspective, or providing moral support (a "thank you" goes a long way).

Users who continue to engage with the project and its community will often become more and more involved. Such Users may find themselves becoming Contributors, as described in the next section.

Users:

- Will abide by the license agreements of Dreamer Labs software

### Contributors

Contributors are community members who contribute in concrete ways to the project, most often in the form of code and/or documentation. Anyone can become a Contributor, and contributions can take many forms. There is no expectation of commitment to the project, no specific skill requirements, and no selection process.

Contributors have read-only access to source code and may submit changes via merge requests. Contributor merge requests have their contribution reviewed and merged by one of the project Maintainers. Maintainers work with Contributors to review their code and prepare it for merging.

As Contributors gain experience and familiarity with the project, their profile within, and commitment to, the community will increase. At some stage, they may find themselves being nominated for maintainership by an existing Maintainer.

Contributors:

- Will abide by the contribution guidelines provided by Maintainers
- Will abide by the decisions of the Maintainers and Technical Steering Committee (TSC)

### Maintainers

Maintainers are community members who have shown that they are committed to the continued development of one or more Dreamer Labs projects through ongoing engagement with the community. Maintainers are given push access to one or more project's repositories and must abide by the organization's technical direction and other policies outlined by the Technical Steering Committee (TSC).

Maintainers are nominated by an existing project Maintainer, and are selected, appointed, and removed (as needed) by the TSC.

Maintainers:

- Will provide Contributors with contribution guidelines for each project they maintain
- Will abide by the decisions of the Technical Steering Committee (TSC)

### Technical Steering Committee (TSC) Members

The TSC has final authority over all activities sponsored by Dreamer Labs including:

- The organization's overall governance and technical direction
- Project code quality standards
- Cross-project coordination efforts
- Maintainer selection and removal

Community member are considered TSC members when:

- They are selected as a Maintainer of a Dreamer Labs projects by the TSC
- They are an employee of Global InfoTek, Inc.

In order for the TSC to fulfill its purpose, members must fulfill additional responsibilities over and above those of a Maintainer. The TSC may only conduct business with quroum. Quoroum is acheieved when a simple majority of TSC members are present during a TSC meeting.

TSC members:

- Should participate in TSC meetings as much as possible
- May propose additions of new Dreamer Labs projects
- May propose archiving of existing Dreamer Labs projects
- May propose changes to governance, process, technical direction, et cetera
- May vote on the selection and removal of Maintainers

#### TSC Meetings

The TSC meets as-needed in the location specified by the meeting facilitator. The facilitator is the TSC member that called the meeting, usually to address one or more items in the TSC agenda item backlog.

Items are usually added to the TSC agenda when they are significant/impactful modifications of governance, technical direction, et cetera.

The intention of the agenda is not to approve or review all patches. That should happen continuously, and is handled by the larger group of project Maintainers. The intention of the committee is also not to micromanage the activities of individual projects. The TSC should only provide direction when asked, or when there is a compelling reason to standardize things across all projects.

Meetings can either be open or closed, at the discretion of the facilitator. Community members may attend any open TSC meeting as a non-voting member. Additionally, any community member may ask that something be added to the next meeting's agenda by logging an issue to this project. The TSC may opt to either defer, address, or refuse any agenda item in the backlog.

At the start of each TSC meeting, the attending members will conduct a review of the agenda item backlog. Members may add any items they like to the agenda item backlog at any time during the meeting. Issues in the agenda backlog will be selected and addressed by order of importance, as determined via the consensus of the TSC members present.

The TSC follows a Consensus Seeking decision making model. When deciding which items to address during a meeting, or when a decision regarding an agenda item has appeared to reach a consensus through discussion, the facilitator will ask "Does anyone object?" as a final call for dissent from the consensus.

If an agenda item cannot reach a consensus, a TSC member can call for either a closing vote or a vote to table the issue to a later meeting. The call for a vote must be approved by a majority of the TSC or else the discussion will continue. Simple majority rules.

The meeting facilitator is responsible for summarizing the discussion of each agenda item addressed during the meeting and commenting on the issue. Issues that are sufficiently resolved will be closed. These issues will act as architectural decision records (ADRs) for the organization, and may spawn other issues in backlogs of individual projects within the organization for Maintainers to address.

### Administrators

Administrators enact any global technical changes requiring elevated permissions beyond those required by that of a Maintainer of a project. Administrators must be TSC members, and are assigned by the TSC via a simple majority vote. The number of administrators should be three at all times to ensure redundancy and continuity.

Administrators:

- Will abide by the decisions of the Technical Steering Committee (TSC)
- Not use administrative privileges to act as a Maintainer for projects they do not maintain

